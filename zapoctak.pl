/** 
Adam Filandr
Zápočtový program - Neprocedurální programování NPRG005
Zadání 522 b) - Převod konečného automatu na regexp + převod regexpu na konečný automat.
SWI-Prolog
 */

/** Reg -> automat */
/** ------------------------------------------------------------------------------------------------------------ */


r(Left, Center, Right).
/** Vzninke strom z r - Left a Right jsou podstromy, Center je buď operátor, nebo hodnota. */
/** U "*" se chovat, jako by byla pravá strana vždycky r(nil,nil,nil) - tim se nasimuluje unární operátor */

/** Nalezne znaky, které se mohou v přijímaném slově vyskytovat na prvním místě (vstupní stavy)*/
find_first(r(nil, C, nil), [C]).
find_first(r(L, "+", R), V):- find_first(L, X), find_first(R, Y), append(X, Y, V).
find_first(r(L, ".", _), V):- find_first(L, V).
find_first(r(L, "*", _), V):- find_first(L, V).

/** Nalezne znaky, které se mohou v přijímaném slově vyskytovat na posledním místě (přijímající stavy) */
find_last(r(nil, C, nil), [C]).
find_last(r(L, "+", R), V):- find_last(L, X), find_last(R, Y),  append(X, Y, V).
find_last(r(_, ".", R), V):- find_last(R, V).
find_last(r(L, "*", _), V):- find_last(L, V).

/** Pomocná funkce na tvoření kombinací mezi dvěma seznamy - kvůli zpracování pravidel pro "*" */
try([],[]).
try([L|Ls],[M|Ms]):- member(M,L), try(Ls,Ms).
komb(L,All) :- findall(M, try(L,M), All). 

/** Najde přepisovací funkci pro regexp */
find_rules(r(nil, _, nil), []).
find_rules(r(L, ".", R), V):- find_last(L, X), find_first(R, Y), append(X, Y, Z), find_rules(L, V1), find_rules(R, V2), append(V1, [Z], M), append(M, V2, V).
find_rules(r(L, "+", R), V):- find_rules(L, V1), find_rules(R, V2), append(V1, V2, V).
find_rules(r(L, "*", _), V):- find_first(L, L1), find_last(L, L2), append([L2], [L1], L21), komb(L21, V1), find_rules(L, V2), append(V1, V2, V).

/** Redukuje strom podle lambdy - na určení, zda automat bere prázdné řetězce */
find_lambda(r(nil, _, nil), nonlambda).
find_lambda(r(_, "*", _), lambda).
find_lambda(r(L, ".", R), V):- find_lambda(L, V1), find_lambda(R, V2), V1==lambda, V2==lambda, V=lambda.
find_lambda(r(L, ".", R), V):- find_lambda(L, V1), find_lambda(R, V2), once(\+ V1==lambda ; \+ V2==lambda), V=nonlambda.
find_lambda(r(L, "+", R), V):- find_lambda(L, V1), find_lambda(R, V2), \+ V1==lambda, \+ V2==lambda, V=nonlambda.
find_lambda(r(L, "+", R), V):- find_lambda(L, V1), find_lambda(R, V2), (V1==lambda ; V2==lambda), V=lambda.

/** Rozhodne, zda automat bere prázdné řetězce */
lambda_out(X, [q0]):- X==lambda.
lambda_out(X, []):- X==nonlambda.

/** Vytvoří páry */
pairs(L1, L2, Pairs):-findall([A,B], (member(A, L1), member(B, L2)), Pairs).

/** Rozloží regexp na vstupní stavy, výstupní stavy a přechodovu funkci */
analyze_reg(Regexp):- 
find_first(Regexp, First), find_last(Regexp, L), find_rules(Regexp, R1), find_lambda(Regexp, V1), lambda_out(V1, V2),
take_states(L, S), append(V2, S, Last),
pairs([[q0, 0]],First, R2),append(R1, R2, R3), make_rules(R3),
take_states(R3, Right), take_letters(R3, Left), append(Left, Right, All),
take_states(All, Sta), take_letters(All, Let), sort(Sta, States), delete(Let, q0, Lette), sort(Lette, Letters),
write("Q = "),write(States), nl, write("X = "), write(Letters), nl, write("F = "), write(Last), nl.


/** Na získání q0, F, Q */
take_right([_, Y], [Y]).
take_states([], []).
take_states([X|Ys], V):-  take_right(X, V2), take_states(Ys, V1), append(V1, V2, V).

/** Na získání X */
take_left([X, _], [X]).
take_letters([],[]).
take_letters([X|Ys], V):- take_left(X, V2), take_letters(Ys, V1), append(V1, V2, V).

/** Získání δ  */
right([_,Y], Y).
left([X,_], X).
calc_pair([X,Y]):- right(X, State1), left(Y, Letter), right(Y, State2), format('δ(~a, ~w) = ~a ~n', [State1, Letter, State2]).
make_rules([]).
make_rules([X | Ys]):- calc_pair(X), make_rules(Ys).


chew([]).
chew([Regexp|Rest]):- \+Regexp==end_of_file, write(Regexp), nl, write("A = (Q, X, δ, q0, F)"), nl, analyze_reg(Regexp), nl, chew(Rest).
chew([Regexp|Rest]):- Regexp==end_of_file, write(Regexp), nl, fail.

read_file(Stream,end_of_file):- at_end_of_stream(Stream).
read_file(Stream,[X|L]):- \+at_end_of_stream(Stream),read(Stream,X),read_file(Stream,L).

regexp(File):- open(File, read, Str), read_file(Str,Lines), chew(Lines), close(Str).


/** Automat -> Regexp */
/** ------------------------------------------------------------------------------------------------------------ */

/** P= pravidla automatu, F=koncové stavy */
a(P, F).

/** Alg na přeměnu seznamu pravidel do DKA */
get_element(0, X, Y, P, V):- once(search_auto(X,Y, P, V)).

/** První je null a druhé ne */
get_element(Level, X, Y, P, V):- Level2 is Level - 1, Level2 >=0, get_element(Level2, X, Y, P, V1), get_element(Level2, X, Level, P, V2), 
get_element(Level2, Level, Level, P, V3), get_element(Level2, Level, Y, P, V4), 
V1==null, \+ V2== null, \+ V3== null, \+ V4== null,
atom_concat('(', V3, H), atom_concat(H, ')*', H1),
atom_concat('(', V2, L), atom_concat(L, ').', L1),
atom_concat('.(', V4, R), atom_concat(R, ')', R1),
atom_concat(L1, H1, V5), atom_concat(V5, R1, V).

/** Některý z druhých je null a první ne */
get_element(Level, X, Y, P, V):- Level2 is Level - 1, Level2 >=0, get_element(Level2, X, Y, P, V1), get_element(Level2, X, Level, P, V2),
get_element(Level2, Level, Y, P, V4), 
\+V1==null, once(V2== null ; V4== null),
atom_concat(V1, '', V).

/** Oboje jsou null */
get_element(Level, X, Y, P,V):- Level2 is Level - 1, Level2 >=0, get_element(Level2, X, Y, P, V1), get_element(Level2, X, Level, P, V2), 
get_element(Level2, Level, Y, P, V4), 
V1==null, once(V2== null ; V4== null), V='null'.

/** Ani jedno není null */
get_element(Level, X, Y, P,V):- Level2 is Level - 1, Level2 >=0, get_element(Level2, X, Y, P, V1), get_element(Level2, X, Level, P,V2), 
get_element(Level2, Level, Level, P, V3), get_element(Level2, Level, Y, P, V4),
\+V1==null, \+ V2== null, \+ V3== null, \+ V4== null,
atom_concat('(', V3, H), atom_concat(H, ')*', H1),
atom_concat('(', V2, L), atom_concat(L, ').', L1),
atom_concat('.(', V4, R), atom_concat(R, ')', R1),
atom_concat(L1, H1, V5), atom_concat(V5, R1, V6),
atom_concat('+', V6, V7), atom_concat(V1, V7, V).

/** První je null a druhé není kromě prostředku */
get_element(Level, X, Y, P, V):- Level2 is Level - 1, Level2 >=0, get_element(Level2, X, Y, P, V1), get_element(Level2, X, Level, P, V2), 
get_element(Level2, Level, Level, P, V3), get_element(Level2, Level, Y, P, V4), 
V1==null, \+ V2== null, V3== null, \+ V4== null,
atom_concat('(', V2, L), atom_concat(L, ')', L1),
atom_concat('.(', V4, R), atom_concat(R, ')', R1),
atom_concat(L1, R1, V).

/** Ani jedno není null kromě prostředku druhého */
get_element(Level, X, Y, P,V):- Level2 is Level - 1, Level2 >=0, get_element(Level2, X, Y, P, V1), get_element(Level2, X, Level, P,V2), 
get_element(Level2, Level, Level, P, V3), get_element(Level2, Level, Y, P, V4),
\+V1==null, \+ V2== null, V3== null, \+ V4== null,
atom_concat('(', V2, L), atom_concat(L, ')', L1),
atom_concat('.(', V4, R), atom_concat(R, ')', R1),
atom_concat(L1, R1, V5),atom_concat('+', V5, V6), atom_concat(V1, V6, V).


/** V pravidlech najde všechny možné stavy */
get_states(P, V):- take_letters(P, V1), take_states(P, V2), take_states(V2, V3), append(V1, V3, V4), sort(V4, V5), delete(V5, 0, V).

grab_left([X, _], X).
grab_right([_, Y], Y).

/** Vezme počáteční hodnotu z pravidel */
search_auto(_,_, [], null).
search_auto(X,Y, [Prav | _], V):- grab_left(Prav, L), grab_right(Prav, R1), grab_right(R1, R), X==L, Y==R, \+ X==Y, grab_left(R1, V).
search_auto(X,Y, [Prav | _], V):- grab_left(Prav, L), grab_right(Prav, R1), grab_right(R1, R), X==L, Y==R, X==Y, grab_left(R1, V1), atom_concat(V1, '+λ', V).
search_auto(X,Y, [Prav | Rest], V):- grab_left(Prav, L), grab_right(Prav, R1), grab_right(R1, R), (\+ X==L; \+ Y==R), search_auto(X,Y, Rest, V).

/** Projde list s final statema a nasčítá výsledky */
/** Total - celkový počet stavů, list jsou final stavy */
construct(_, [], _, A, A).
construct(P, [X | Ys], Total, '', V):- once(get_element(Total, 1, X, P, V1)), construct(P, Ys, Total, V1, V).
construct(P, [X | Ys], Total, A, V):- \+ A=='null', once(get_element(Total, 1, X, P, V1)), atom_concat(A, '+', A2), atom_concat(A2, V1, V2), construct(P, Ys, Total, V2, V).
construct(P, [X | Ys], Total, A, V):- A=='null', once(get_element(Total, 1, X, P, V1)), construct(P, Ys, Total, V1, V).


/** Vypíše regexp pro automat */
analyze_aut(end_of_file, '').
analyze_aut(a(P,F), V):- get_states(P, States), length(States, Count), construct(P, F, Count, '', V).

eval(X):- \+ X=='', write(X).
eval(X):- X=='null', write("Regular expresion doesn't exist for this automaton.").

nom([]).
nom([Auto|Rest]):- \+ Auto==end_of_file, write(Auto), nl, convert(Auto, Automat),write(Automat), nl, analyze_aut(Automat, V), eval(V), nl, nl, nom(Rest).
nom([Auto|Rest]):- Auto==end_of_file, write(Auto), nl, fail.

automaton(File):- open(File, read, Str), read_file(Str,Lines), nom(Lines), close(Str).


/** Konvertor NKA -> DKA: */
/** ------------------------------------------------------------------------------------------------------------ */

/** Všechny počáteční stavy */
grab_states([X, [_, Z]], V):- append([[X]], [[Z]], V).
all_states([],[]).
all_states([X|Ys],V):- grab_states(X, V1), all_states(Ys, V2), append(V1, V2, V). 

/** Všechny počáteční písmena */
grab_letters([_, [Y, _]], [Y]).
all_letters([],[]).
all_letters([X|Ys],V):- grab_letters(X, V1), all_letters(Ys, V2), append(V1, V2, V).
 

/** Vyhledání nových stavů */
state_letter([], _, _, []).
state_letter([[X, [Y, Z]] | Ps], S, L, V):- S==X, Y==L, state_letter(Ps, S, L, V2), append([Z], V2, V).
state_letter([[X, [Y, _]] | Ps], S, L, V):- once(\+S==X ; \+Y==L), state_letter(Ps, S, L, V2), append([], V2, V).

use_rules(_, [], _, []).
use_rules(P, [S|Ss], L, [V]):- state_letter(P, S, L, V1), use_rules(P, Ss, L, V2), append(V1, V2, V), \+ V==[].
use_rules(P, [S|Ss], L, V):- state_letter(P, S, L, V1), use_rules(P, Ss, L, V2), append(V1, V2, V), V==[].

merge(_, _, [], []).
merge(P, Smnoz, [L|Ls], V):- use_rules(P, Smnoz, L, V1), flatten(V1, V3),sort(V3, V4), merge(P, Smnoz, Ls, V2), \+V4==[],append([V4], V2, V).
merge(P, Smnoz, [L|Ls], V):- use_rules(P, Smnoz, L, V1), flatten(V1, V3),sort(V3, V4), merge(P, Smnoz, Ls, V2), V4==[],append(V4, V2, V).

new_states(_, [], _, []).
new_states(P, [Smnoz | Smnozs], Lmnoz, V):- merge(P, Smnoz, Lmnoz, V1), new_states(P, Smnozs, Lmnoz, V2), append(V1, V2, V).


/** Vyhledání nových pravidel */
search_rules(_, [], _, []).
search_rules(P, [S|Ss], L, V):- state_letter(P, S, L, V1), search_rules(P, Ss, L, V2), append(V1, V2, V), \+ V==[].
search_rules(P, [S|Ss], L, V):- state_letter(P, S, L, V1), search_rules(P, Ss, L, V2), append(V1, V2, V), V==[].

merge_rules(_, _, [], []).
merge_rules(P, Smnoz, [L|Ls], V):- search_rules(P, Smnoz, L, V1), sort(V1, V3), merge_rules(P, Smnoz, Ls, V2), append([[Smnoz, [L, V3]]], V2, V).

new_rules(_, [], _, []).
new_rules(P, [Smnoz | Smnozs], Lmnoz, V):- merge_rules(P, Smnoz, Lmnoz, V1), new_rules(P, Smnozs, Lmnoz, V2), append(V1, V2, V).


/** Nové final stavy */
check(S, F, [S]):- intersection(S, F, V1), \+V1==[].
check(S, F, []):- intersection(S, F, V1), V1==[].

new_finals([], _, []).
new_finals([S | Ss], F, V):- check(S, F, V1), new_finals(Ss, F, V2), append(V1, V2, V).


/** Přejmenování všeho */
left_side([], _, _, []).
left_side([[X, [Y, Z]]|Ps], S, Number, V):- X==S, left_side(Ps, S, Number, V1), append([[Number, [Y, Z]]], V1, V).
left_side([[X, [Y, Z]]|Ps], S, Number, V):- \+X==S, left_side(Ps, S, Number, V1), append([[X, [Y, Z]]], V1, V).

right_side([], _, _, []).
right_side([[X, [Y, Z]]|Ps], S, Number, V):- Z==S, right_side(Ps, S, Number, V1), append([[X, [Y, Number]]], V1, V).
right_side([[X, [Y, Z]]|Ps], S, Number, V):- \+Z==S, Z==[], right_side(Ps, S, Number, V1), append([], V1, V).
right_side([[X, [Y, Z]]|Ps], S, Number, V):- \+Z==S, \+ Z==[], right_side(Ps, S, Number, V1), append([[X, [Y, Z]]], V1, V).

rename_finals([], _, _, []).
rename_finals([F| Fs], S, Number, V):- F==S, rename_finals(Fs, S, Number, V1), append([Number], V1, V).
rename_finals([F| Fs], S, Number, V):- \+ F==S, rename_finals(Fs, S, Number, V1), append([F], V1, V).

rename(_, P, [], F, P, F).
rename(Number, P, [S | Ss], F, Pf, Ff):- left_side(P, S, Number, V1), right_side(V1, S, Number, P2), rename_finals(F, S, Number, F2),
Number2 is Number + 1, rename(Number2, P2, Ss, F2, Pf, Ff).

/** Přepsání množin stavů na čísla a určení finálních stavů */
finish(P, S, L, F, a(Pf, Ff)):- once(new_rules(P, S, L, P2)), new_finals(S, F, F2),
write("New rules: "), write(P2), nl,
write("New finals: "), write(F2), nl,
write("After renaming: "), nl,
rename(1, P2, S, F2, Pf, Ff).

scan(P, S, L, F, V):- new_states(P, S, L, S2), append(S, S2, S3), sort(S3, S4),
\+ S==S4, scan(P, S4, L, F, V).
scan(P, S, L, F, V):- new_states(P, S, L, S2), append(S, S2, S3), sort(S3, S4), S==S4, 
write("New states: "), write(S), nl,
finish(P, S, L, F, V).

convert(a(P,F), V):- all_states(P, V1), sort(V1, S), all_letters(P, V2),sort(V2, L),
write("Original rules"), write(P), nl,
write("All states: "), write(S), nl,
write("All letters: "), write(L), nl,
scan(P, S, L, F, V).
