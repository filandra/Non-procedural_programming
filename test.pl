/**Fronta, zásobník add, get, delka */

add_first(X, [], [X]).
add_first(X, [Y | Ys], [X,Y | Ys]) :- add_first(X, Ys, [X | Ys]).

pop_zas([X|Xs], Xs, X).

pop_fro([X], [], X).
pop_fro([X | Xs], [X | Ys], Y):- pop_fro(Xs, Ys, Y).


delka([], A, A).
delka([X|Xs], B, A):- C is B + 1, delka(Xs, C, A).

/** Binární strom, in, add, remove */

t(L,V,R).

in(X, t(_,X,_)).
in(X, t(L,C,_)):- X<C , in(X,L).
in(X, t(_,C,R)):- X>C, in(X,R).

% Přidá prvek, který ještě není ve stromě.
add_new(X, nill, t(nill, X, nill)).
add_new(X, t(L,C,R), t(L,C,Y)):- X>C, add_new(X, R, Y).
add_new(X, t(L,C,R), t(Y,C,R)):- X<C, add_new(X, L, Y).
add_new(X, t(nil,C,R), t(t(nil,X,nil),C,R):- X<C.
add_new(X, t(L,C,nil), t(L,C,t(nil,X,nil))):- X>C.

% Odebere node.
rem_node(X, t(L, X, R), nill).
rem_node(X, t(L,C,R), t(Y,C,R)):- X<C, rem_node(X, L, Y).
rem_node(X, t(L,C,R), t(L,C,Y)):- X>C, rem_node(X, R, Y).

